package ru.t1consulting.vmironova.tm.api.repository;

import ru.t1consulting.vmironova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    Project create(String name, String description);

    Project create(String name);

    boolean existsById(String id);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void clear();

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    int getSize();

}
